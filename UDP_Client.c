//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/time.h>

#define PORT     4240
#define MAXLINE 1024

// Driver code
int main() {
    int sockfd; //Socket descriptor, like a file-handle
    char buffer[MAXLINE]; //buffer to store message from server
    char *hello = "Hello from client"; //message to send to server
    struct sockaddr_in servaddr;  //we don't bind to a socket to send UDP traffic, so we only need to configure server address
    char userInputOpt[500];
    char songstart[1] = "\n";
    char userIn[500] = "\n";
//    int  = 0;

    while (strcmp(userInputOpt, "3") != 0) {


        //print options to command line
        printf("%s\n", "1) List");
        printf("%s\n", "2) Stream");
        printf("%s\n", "3) Quit\n");
        scanf("%s", userInputOpt);
//        printf("User chose: %s\n", userInputOpt);

        if (strcmp(userInputOpt, "1") == 0 || strcmp(userInputOpt, "2") == 0) {
            // Creating socket file descriptor
            if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
                perror("socket creation failed");
                exit(EXIT_FAILURE);
            }

            // Filling server information
            servaddr.sin_family = AF_INET; //IPv4
            servaddr.sin_port = htons(
                    PORT); // port, converted to network byte order (prevents little/big endian confusion between hosts)
            servaddr.sin_addr.s_addr = INADDR_ANY; //localhost


            //send request to the server

            //option one, request songs
            if (strcmp(userInputOpt, "1") == 0) {
                strcpy(userInputOpt, "LIST_REQUEST");
            }else if(strcmp(userInputOpt, "2") == 0){
                strcpy(userInputOpt, "START_STREAM");
         //need to figure out how to take in the full string, not just to the space?
                scanf("%s",userIn);
                strcat(songstart, userIn);
                strcat(userInputOpt,songstart);
            }

            int n, len = sizeof(servaddr);
            sendto(sockfd, (const char *) userInputOpt, strlen(userInputOpt), 0,
                   (const struct sockaddr *) &servaddr, sizeof(servaddr));    //send request to server
            printf("Request for song list sent.\n");

            // Set the timeout for the socket:
            struct timeval timeout; //structure to hold our timeout
            timeout.tv_sec = 5; //5 second timeout
            timeout.tv_usec = 0; //0 milliseconds
            if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(timeout)) < 0) {
                perror("setsockopt failed");
                exit(EXIT_FAILURE);
            }
// ...address configuration and possibly a send command would happen here...

// Receive data with a timeout:
//            n = recvfrom(sockfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &servaddr, &len);
            if ((n = recvfrom(sockfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &servaddr, &len)) < 0) {
                // Handle errors here.  Errno 11, "Resource Temporarily Unavailable" is returned as a result of a timeout.
                perror("ERROR");
                printf("Errno: %d. ", errno);
                exit(EXIT_FAILURE);
            }

            //Sending message to server
//            sendto(sockfd, (const char *) hello, strlen(hello), 0, (const struct sockaddr *) &servaddr,
//                   sizeof(servaddr));
//            printf("Hello message sent.\n");

//            for (int i - 0; i <)

        buffer[n] = '\0'; //terminate message
        printf("%s\n", buffer);

        }
        if (strcmp(userInputOpt, "3") == 0) {
            sendto(sockfd, (const char *) userInputOpt, strlen(userInputOpt), 0, (const struct sockaddr *) &servaddr, sizeof(servaddr));    //send request to server
            printf("Exiting now...");
            close(sockfd);
            return 0;
        }


    }
}