//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <time.h>

#define PORT 4240
#define MAXLINE 1024
int main(){

    int socketfd;  //Socket descriptor, like a file-handle
    struct sockaddr_in srvaddr, cliaddr; //Stores IP address, address family (ipv4), and port
    char buffer[MAXLINE]; //buffer to store message from client
    char *hello = "Hello from server"; //message to respond to client
    char *songs = "LIST_REPLY\nSuzanne Vega - Toms Diner.mp3\nBilly Joel - We Didn't Start the Fire.mp3\n";
    char *whichsong = "Which song?";
    char *error = "COMMAND_ERROR\nPlease enter 1, 2, or 3.";
    int userIn = 0;

    // Creating socket file descriptor
    // AF_INET = "domain" = IPv4
    // SOCK_DGRAM = "type" = UDP, unreliable
    // protocol=0, specifies UDP within SOCK_DGRAM type.
    if ((socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    srvaddr.sin_family = AF_INET;           // IPv4
    srvaddr.sin_addr.s_addr = INADDR_ANY;   // All available interfaces
    srvaddr.sin_port = htons( PORT );       // port, converted to network byte order (prevents little/big endian confusion between hosts)



    // Forcefully attaching socket to the port 8080
    // Bind expects a sockaddr, we created a sockaddr_in.  (struct sockaddr *) casts pointer type to sockaddr.
    if (bind(socketfd, (const struct sockaddr *)&srvaddr, sizeof(srvaddr))<0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

// Receive message from client
    while (1) {
        int len = sizeof(cliaddr), n; //must initialize len to size of buffer
        if ((n = recvfrom(socketfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &cliaddr, &len)) < 0) {
            perror("ERROR");
            printf("Errno: %d. ", errno);
            exit(EXIT_FAILURE);
        }
        buffer[n] = '\0';  //terminate message
        printf("Client chose: %s\n", buffer);

        //    int input = strcmp(userInputOpt,2);

        if (strcmp(buffer, "LIST_REQUEST") == 0) {             //check to make sure user inputs are the desired inputs
            userIn = 1;                             //set userIn variable to 1 for list
      //print out the user's input
            sendto(socketfd, (const char *) songs, strlen(songs), 0, (const struct sockaddr *) &cliaddr,len);   //send song1 name to client
//            sendto(socketfd, (const char *)song2, strlen(song2), 0, (const struct sockaddr *) &cliaddr, len);   //send song2 name to client
        } else if (strcmp(buffer, "START_STREAM") == 0) {
//            sendto(socketfd, (const char *) whichsong, strlen(whichsong), 0, (const struct sockaddr *) &cliaddr,len);   //send song1 name to client
            //2 options, stream Suzanne Vega - Toms Diner.mp3 or Billy Joel - We Didn't Start the Fire.mp3

            //if START_STREAM\nSuzanne Vega - Toms Diner.mp3.
                //senf file with first song

            //if START_STREAM\nBilly Joel - We Didn't Start the Fire.mp3
                //send file with first song




//                STREAM_DATA\n65AC3F03 use this RFC to where each two digit hexadecimal number following STREAM_DATA\n represents a
//                single byte of MP3 frame data.

//                STREAM_DONE use this when A STREAM_DONE message is sent by the server when the stream is complete. Until
//                then, a client that has issued a START_STREAM command should assume the stream
//                is continuing until an appropriate time-out occurs (5 seconds).

            userIn = 2;
            printf("Client : %s\n", buffer);
        } else if (strcmp(buffer, "3") == 0) {
            userIn = 3;
            printf("Client : %s\n", buffer);
        } else {
//            sendto(socketfd, (const char *) error, strlen(error), 0, (const struct sockaddr *) &cliaddr, len);


        }

//    printf("Client : %s\n", buffer);

        // Respond to client
//        sendto(socketfd, (const char *) hello, strlen(hello), 0, (const struct sockaddr *) &cliaddr, len);

        if (userIn == 3) {
            printf("Socket closing\n");
            close(socketfd); // Close socket
            break;     //break out of while loo[\p
        }
    }
    return 0;
}